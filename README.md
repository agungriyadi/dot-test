## Description

[Nest](https://github.com/nestjs/nest) framework TypeScript starter repository.

## Installation

```bash
$ npm install
```

## Running the app

```bash
# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
$ npm run start:prod
```

## Test

```bash
# unit tests
$ npm run test

# e2e tests
$ npm run test:e2e

# test coverage
$ npm run test:cov
```

## DOC

```bash
get users
url: /users
method: GET

add user
url: /users
method: POST
data req: { username: 'agung', password: 'riyadi' }

edit user
url: /users/:id
method: PUT
data req: { username: 'dot', password: 'test' }

edit password
url: /users/:id
method: PUT
data req: { password: 'testPassword' }

delete user
url: /users/:id
method: DELETE

add post
url: /users/:id/posts
method: POST
data req:
{
title: 'test title',
description: 'test description',
}

add profile
url: /users/:id/profiles
method: POST
data req:
{
firstName: 'udin',
lastName: 'ahmad',
age: 22,
dob: 'bekasi, 10 jan 2000'
}
```