import { Test, TestingModule } from '@nestjs/testing';
import { INestApplication, HttpStatus } from '@nestjs/common';
import * as request from 'supertest';
import { AppModule } from './../src/app.module';

describe('UsersController (e2e)', () => {
  let app: INestApplication;

  beforeEach(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();

    app = moduleFixture.createNestApplication();
    await app.init();
  });

  // it('/ (GET)', () => {
  //   return request(app.getHttpServer())
  //     .get('/')
  //     .expect(200)
  //     .expect('Hello World!');
  // });
  it('/users (GET)', () => {
    return request(app.getHttpServer())
      .get('/users')
      .expect(200)
      .expect(HttpStatus.OK);
  });

  it('/users (POST)', () => {
    return request(app.getHttpServer())
      .post('/users')
      .send({ username: 'agung', password: 'riyadi' })
      .expect(201)
      .expect(HttpStatus.CREATED);
  });

  it('/users/:id (PUT)', () => {
    return  request(app.getHttpServer())
      .put('/users/1')
      .send({ username: 'dot', password: 'test' })
      .expect(200)
      .expect(HttpStatus.OK);
  });

  it('/users/:id (PATCH)', () => {
    return request(app.getHttpServer())
      .patch('/users/1')
      .send({ password: 'test2' })
      .expect(200)
      .expect(HttpStatus.OK);
  });

  it('/users/:id (DELETE)', () => {
    return request(app.getHttpServer())
      .delete('/users/1')
      .expect(200)
      .expect(HttpStatus.OK);
  });

  it('/users (POST)', () => {
    return request(app.getHttpServer())
      .post('/users')
      .send({ username: 'agung', password: 'riyadi' })
      .expect(201)
      .expect(HttpStatus.CREATED);
  });

  it('/users/:id/profile (POST)', () => {
    return request(app.getHttpServer())
      .post('/users/2/profiles')
      .send({ 
        firstName: 'udin',
        lastName: 'ahmad',
        age: 22,
        dob: 'bekasi, 10 jan 2000'
      })
      .expect(201)
      .expect(HttpStatus.CREATED);
  });
  it('/users/:id/post (POST)', () => {
    return  request(app.getHttpServer())
      .post('/users/2/posts')
      .send({ 
        title: 'test title',
        description: 'test description',
       })
      .expect(201)
      .expect(HttpStatus.CREATED);
  });
});
